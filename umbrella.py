class Umbrella:
    x: int
    y: int

    def __init__(self, x: int, y: int, built:bool = False):
        def check_coords(coord):
            if (coord < 0) or (coord > 256) or (type(coord) != int):
                raise ValueError('Not a valid coord')

        check_coords(x)
        check_coords(y)
        self.x = x
        self.y = y
        self.image = (0,48,0,16,16)
        self.built = built
    @property
    def x(self):
        return self.__x
    @x.setter
    def x(self, x):
        self.__x = x
