import pyxel
class Block:
    x: int
    y: int

    def __init__(self, x, y, destructible: bool = False):
        def check_coords(coord):
            if (coord < 0) or (coord > 256) or (type(coord) != int):
                raise ValueError('Not a valid coord')
        check_coords(x)
        check_coords(y)
        self.selection_image = (0, 16, 0, 16, 16)
        self.x = x
        self.y = y
        self.destructible = destructible
    @property
    def image(self):
        '''This attribute returns the correct image depending on whether the block is destructible or not'''
        if self.destructible:
            return (0,32,112,16,16)
        else:
            return (0, 16, 112, 16, 16)
