class Lemming:
    '''This class represents a lemming '''
    x: int
    y: int
    mov:str #R,L,Asc,Desc,Fal,Umb,Ded, Bl

    # do not confuse blocker attribute with Blocker class
    def __init__(self, x, y, mov, building:bool = False):
        """ Init checks if the values provided to the object are correct"""
        # x and y can only be between 0 and 256, otherwise it will raise an error
        def check_coords(coord):
            if (coord < 0) or (coord > 256) or (type(coord) != int):
                raise ValueError('Not a valid coord')
        check_coords(x)
        check_coords(y)


        # if they raise no errors, then we can add them as attributes
        self.x = x
        self.y = y
        self.mov=mov
        self.prev_h: int
        self.prev_dir=mov
        self.prev_building: bool = False
        self.blocker: bool = False  # is the lemming acting as blocker?

        # groups the movements into left and right movements
        self.__left_movements = ('L', 'AscL', 'DescL')
        self.__right_movements = ('R', 'AscR', 'DescR')

    # this is to draw the corresponding sprites
    @property
    def image(self):
        # everytime the lemming is moving leftwards. Or if it is falling and it was previously moving leftwards
        if self.mov in self.__left_movements or (self.prev_dir in self.__left_movements and self.mov in ('Fal',"Builder")):
            return (0, 40, 32, 8, 8)
        # everytime the lemming is moving rightwards. Or if it is falling and it was previously moving rightwards
        elif self.mov in self.__right_movements or (self.prev_dir in self.__right_movements and self.mov in ('Fal',"Builder")):
            return (0, 40, 24, 8, 8)

        # returns the image of the lemming with the umbrella
        elif self.prev_dir in self.__left_movements and self.mov == 'Umb':
            return (0,40,44,8, 12)
        elif self.prev_dir in self.__right_movements and self.mov == 'Umb':
            return (0,32,44,8, 12)
        # dead lemming
        elif self.mov == 'Ded':
            return (0,40,80,8,8)
        elif self.mov=="Blocker":
            return (0,32,56,8,8)
        else:
            return (0, 32,32, 8, 8)
    @property
    def mov(self):
        return self.__mov
    # Each time the lemming changes movement, the building attribute should be set to false
    @mov.setter
    def mov(self, mov):
        self.building = False
        self.__mov = mov


